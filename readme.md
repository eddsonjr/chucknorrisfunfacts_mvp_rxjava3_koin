# Chuck Noris Fun Facts - MVP - RXJava 3

Aplicativo desenvolvido em Android/Kotlin cujo objetivo é consumir a API chucknorris.io. O aplicativo permite visualizar piadas a respeito do Chuck Noris e realizar buscas por piadas, baseadas em palavras chave (em inglês).

![alt](/uploads/a9ea2f716bb333a415625b6fe9dc9dcd/screens.png)


## API

**chucknorris.io** é uma API baseada em JSON cujo o objetivo é disponibilizar pequenos textos satíricos (fun facts) sobre o ator Chuck Norris.  Ela é capaz de retornar um único texto satírico de forma randômica, fazer buscas por textos satíricos baseado em uma lista de categorias ou por algum texto qualquer que lhe é fornecida.   
  
Para maiores detalhes, consulte a [documentação](https://api.chucknorris.io/). 

## Apresentação da Aplicação

A aplicação conta com as seguintes funcionalidades: 

1. **Somente um Joke** - Apresenta ao usuário um único texto satírico sobre Chuck Noris por vez. Este texto é gerado de forma randômica pela API e o usuário pode requisitar a geração de mais de uma *fun fact*.

2. **Lista de Categorias de Jokes** - Solicita da API a lista de categorias nos quais uma *fun fact* pode ser gerada. Ao selecionar uma das categorias, o usuário poderá requisitar *fun facts*  baseadas naquela categoria específica.

3. **Procurar Jokes por Texto** - Nesta funcionalidade, o usuário pode solicitar uma lista de *fun facts* baseadas em uma pesquisa por texto livre. Por exemplo, caso o usuário procure pela palavra *dog*, o aplicativo irá solicitar da API que retorne uma lista de *fun facts* baseados no texto inserido. 

## Tecnologias e Arquitetura

O aplicativo conta com o uso das seguintes tecnologias e arquitetura:
 - Arquitetura MVP (Model - View - Presenter)
 - Padrão Repository
 - RXJava 3 
 - Retrofit / OKHttp / GSON
 - Fragments (sem Navigation Component) / Dialog Fragment
 - Saved Instance
 - Koin (injeção de dependências)

Abaixo há seções que explicam cada um dos tópicos acima citados.

**Outra versão do App:** Existe uma outra versão do aplicativo desenvolvida com arquitetura MVVM e Coroutines. Esta versão está disponível [aqui](https://gitlab.com/eddsonjr/chucknorrisfunfacts) (vide branch _master_).

#### Branchs
 - **Main:** Projeto com todas as funcionalidades e documentação incluso.
 - **savedInstance:** Projeto com a funcionalidade de salvar instâncias das fragments e acesso à API.
 - **master:** Projeto somente com acesso à API, sem funcionalidade de salvar instâcnias do fragment e documentação.

### Dependências

Lista de dependências usadas: 


```

    //retrofit com gson e okhttp
    implementation 'com.squareup.retrofit2:retrofit:2.9.0'
    implementation 'com.google.code.gson:gson:2.8.8'
    implementation 'com.squareup.retrofit2:converter-gson:2.9.0'
    implementation 'com.squareup.okhttp3:logging-interceptor:4.9.0'


    //dependencias do koin
    implementation "io.insert-koin:koin-android:3.1.5"


    //RXJava
    implementation "io.reactivex.rxjava3:rxjava:3.0.0"
    implementation "com.github.akarnokd:rxjava3-retrofit-adapter:3.0.0"
    implementation 'io.reactivex.rxjava3:rxandroid:3.0.0'


```

---

## Tecnologias e Arquitetura - Abordagem

Nesta seção serão abordadas explicações básicas sobre a arquitetura e as bibliotecas utilizadas no projeto.

### MVP

*MVP* é o acrônimo para Model - View - Presenter,  corresponde uma arquitetura de projeto constituida das seguintes camadas: 

 - *Model:* Camada que serve para descrever a lógica/regras de negócio da aplicação. Esta camada também é descrita como a mantenedora e fornecedora dos dados necessários na aplicação. 
 
 - *View*: Corresponde a camada que interage com o usuário, responsável pela entrada e saída dos dados.
 
 - *Presenter*: É a camada intermediária, responsável por interligar as camadas de _View_ e _Model_. Essa camada é conhecedora de ambas as outras na arquitetura, responsável por adquirir os dados vindos da camada de _view_, aplicar alguma possível regra caso seja necessário, e por fim passar tais dados para a camada de _model_. Ela também responde no sentido inverso, capturando os dados advindos da camada de _model_, tratando-os e posteriormente enviando-os para a camada de _view_. A nível de implementação Android, o _presenter_ geralmente é constituído de interfaces e classes que implementam tais interfaces, sendo comum o uso de interfaces para representar e controlar os estados da view, bem como o uso de interfaces e classes para manipular os dados e trabalhá-los em conjunto com a camada de _model_.

Na forma de diagrama, o MVP pode ser representado como: 

![alt](/uploads/b4def80578e0500a427355213d851bf7/mvp_basico_diagrama.png)

Como mencionado, a camada de _presenter_ geralemnte é constituida de classes e interfaces responsáveis por realizar a comunicação entre a camada de _view_ e a de _model_. Quando se trata de comunicação com a _view_, no _presenter_ geralmente temos uma interface responsável por indicar os estados da view. No caso do projeto em questão há a interface descrita abaixo, que é implementada na camada de _view_: 

```
    interface BaseViewPresenter {
        fun showLoading()
        fun hideLoading()
        fun showError()
        fun hideError()
        fun <T> showSuccess(data: T)
    }

```

Com isso, o presenter consegue indicar os estados da _view_ com base em respostas vindas do _model_. 
Para a comunicação com o _model_, o _presenter_ também conta com interfaces, além de classes específicas para implementá-las, além  do conhecimento da camada de _view_: 



```
   
    interface GetListOfCategoriesPresenter {
        val view: BaseViewPresenter
        fun requestForCategoryList()

    }


    class GetListOfCategoriesPresenterImpl(
        override val view: BaseViewPresenter,
        private val repository: BaseRepository): GetListOfCategoriesPresenter, NetworkResultListener {

    private val TAG = this.javaClass.name

    override fun <T> onSuccess(data: T) {
        Log.d(TAG,"Request for list of categories returned successfully. Data: ${data.toString()}")
        view.hideLoading()
        view.hideError()
        view.showSuccess(data)
    }

    override fun onFailure() {
        Log.e(TAG,"Request for list of categories failed! ")
        view.hideLoading()
        view.showError()
    }

    override fun requestForCategoryList() {
        Log.d(TAG,"Requesting repository for list of jokes...")
        view.showLoading()
        repository.getCategoriesOfJokes(this)
    }


}

```

Perceba que na interface _GetListOfCategoriesPresenter_ e na classe _GetListOfCategoriesPresenterImpl_ há uma referência à _BaseViewPresenter_. Isso se dá pelo fato da capacidade do presenter de adquirir dados da camada de _model_ e com base nesses dados indicar como a _view_ será apresentada.

O código acima descreve uma maneira de implementação do _presenter_, no entanto podem existir outras maneiras similares de realizar o mesmo trabalho. 


### Padrão Repository

É sabido que a camada de _model_ dentro do MVP é responsável pelos dados e regras de negócio. No entanto, é possível implementar o padrão _Repository_ em conjunto com o MVP. 

O padrão _Repository_ tem como principal objetivo a abstração da aquisição e/ou persistência dos dados. Ele serve para isolar a responsabilidade de aquisição (e salvamento, caso necessário) dos dados das demais classes, tais como entidades. A grande vantagem disso é que as classes que trabalham com regras de negócio não precisam se preocupar em como adquirir tais dados, sendo esta uma função delegada diretamente ao _repository_. Isso permite o desacoplamento e também a facilidade da troca de tecnologias com relação à aquisição dos dados (ex: trocar o retrofit por outra lib) e/ou salvamento (ex: trocar o Room por Realm). 

O padrão _repository_ pode vir também atrelado a um _Data Source_. Geralmente se utiliza _data sources_ quando se há mais de uma maneira de adquirir os dados, como por exemplo, através de uma requisição web ou via banco de dados local. 

Este aplicativo conta com o padrão _Repository_ atrelado a um _Data Source_, porém apenas adquire dados via Rest API, através do Retrofit.   
  
Caso não haja a implementação do padrão _Repository_, a aquisição de dados pode ser feita através de alguma determinada classe dentro da camada de _model_, tal como uma _"APIHelper"_. Já no que diz respeito à banco de dados, pode-se pensar na implementação do padrão _DAO_ ou a implementação de alguma classe que faça uso do Room ou algum ORM, tal como Realm.

Diagralmente temos: 

![alt](/uploads/0a2776e31ac86f2cccf9deb9eb9ed463/MVP_Repository_diagram.png)


Perceba que no código listado acima, a classe _GetListOfCategoriesPresenterImpl_ recebe como parâmetro (injeção) o _BaseRepository_ e que na no método _requestForCategoryList()_ há a chamada para o repository: 

```
    repository.getCategoriesOfJokes(this)
```

Já na classe de _repository_, há a injeção do _data source_ e por fim este último é responsável por realizar as chamadas de API: 


```
    class BaseRepositoryImpl(private val dataSource: BaseDataSource): BaseRepository {

        private val TAG = this.javaClass.name

        override fun getOneJoke(listener: NetworkResultListener) {
            doSubscribeObservable(dataSource.getOneJoke(),listener)
        }

        override fun getCategoriesOfJokes(listener: NetworkResultListener) {
            doSubscribeObservable(dataSource.getCategoriesOfJokes(),listener)
        }

        override fun queryFunFactByCategory(category: String, listener: NetworkResultListener) {
            doSubscribeObservable(dataSource.queryFunFactByCategory(category),listener)
        }

        //.... 

    }


    class BaseDataSourceImpl(private val api: EndPointService): BaseDataSource {

        private val TAG = this.javaClass.name

        override fun getOneJoke(): Observable<Joke?> {
            Log.d(TAG,"Requesting API for One Joke...")
            return api.getRandonFunFact()
        }
    
        override fun getCategoriesOfJokes(): Observable<List<String>?> {
            Log.d(TAG,"Requesting API for Joke Categories...")
           return api.getListOfCategories()
        }

        override fun queryFunFactByCategory(category: String): Observable<Joke?> {
            Log.d(TAG,"Quering Fun Facts by Category. Category selected: $category")
           return api.queryFunFactByCategory(category)
        }

        override fun queryFunFactByText(queryText: String): Observable<ListOfJokes?> {
            return api.queryFunFactByText(queryText)
        }
}

```


### Retrofit e GSON

**Retrofit** - Biblioteca para trabalhar com requisições web REST API. Veja mais na documentação da [Retrofit](https://square.github.io/retrofit/)

**GSon** - Biblioteca utilizada para serializar e desserializar objetos Kotlin/Java para JSON e vice versa. Consulte a documentação da [Gson](https://github.com/google/gson) para maiores detalhes.


### RXJava 

O RXJava é uma lib que permite trabalhar com fluxo de dados assincronos, sendo que este fluxo pode ser criado por qualquer thread e consumido por múltiplos objetos em threads diferentes.  Ele é utilizado em situações de chamadas assíncronas, tais como requisições HTTP, manipulação de banco de dados, tratamento de dados advindos de sensores do dispositivo, controles de UI, entre outros. O RXJava é baseado totalmente no padrão de projeto _Observer_, no qual você configura ele para "escutar" algo e quando há alguma "resposta", ele realiza algum processamento baseado nesta resposta. 

O RXJava conta com os seguintes elementos fundamentais:   
  
 - **Observables**: É um tipo de emissor, servindo para enviar um fluxo de dados (geralmente de forma sequencial). Um _observable_ é consumido por um _observer_. 
 
 - **Observer**: são objetos que consomem dados emitidos por _observables_. Você pode definir múltiplos _observers_ para um único _observable_. O objetos desse tipo devem tratar tanto erros quanto as eventuais respostas relacionadas aos dados que são captados. Um _observer_ também sabe quando não há mais dados sendo transmitidos via _observables_. 

#### Formas de operação do RXJava

Como mencionado, o RXJava trabalha baseado no padrão _Observer_, onde os _observables_ são emissores de dados e os _observers_ são os consumidores. O RXJava trabalha com algumas estruturas de "blocos", que são responsáveis por fazer tratativas e aquisição dos dados que são emtidos. Tais "blocos" são:

 - **onNext**: é chamado a cada item novo emitido por um _Observable_.
 - **onError**: é chamado quando a ocorre um erro qualquer. Neste ponto, a fluxo de dados é interrompido. 
 - **onComplete**: é chamado quando todos os dados do fluxo forem emitidos. Este bloco pode ou não ser implementado. 

Alguns códigos de exemplo: 

```

private fun rxJavaTest1() {

        val p = Pessoa("Fran")

        val listOfPessoas = listOf(
            Pessoa("Fran"),
            Pessoa("Sabrina"),
            Pessoa("Olesia")
        )

        //Criando um observable de pessoa. Um observable e um objeto que pode ser observavel
        val pessoaObservable: Observable<Pessoa> = Observable.just(p) //cria um observavel para somente um objeto

        val pessoaObservable: Observable<List<Pessoa>> = Observable.just(listOfPessoas)
        //Ao usar somente o just com um array, o onNext ira tratar o array como um unico objeto
        //Observando ele por completo



        //Criando um objeto do tipo observer, responsavel por observar um observavel de pessoa
        val pessoaObserver: Observer<List<Pessoa>> = object : Observer<List<Pessoa>> {

            override fun onSubscribe(d: Disposable?) {
                Log.d(TAG,"OnSubscribe")
            }

            override fun onError(e: Throwable?) {
                Log.d(TAG,"Oops! Ocorreu um erro")
            }

            override fun onComplete() {
                Log.d(TAG,"Todos objetos enviados e observados - onCompleted")
            }

            override fun onNext(t: List<Pessoa>?) {
                Log.d(TAG,"Pessoas: $t")
            }

        }
            //Vincula o observer ao observable, fazendo com que um escute o outro
            pessoaObservable.subscribe(pessoaObserver)

        }


    //--------------------

    private fun rxJavaTest3() {
        val p = Pessoa("Edson Jr")

        //o just observa um unico objeto uma unica vez
        Observable.just(p).subscribe {
           println(it.name) //onNext chamado aqui

        }
    }



```


No kotlin é possível ter uma "versão mais curta" do código acima: 

```
    private fun rxJavaTest2() {

        val listOfPessoas = listOf(
            Pessoa("Fran"),
            Pessoa("Sabrina"),
            Pessoa("Olesia")
        )

         //ao usar o fromIterable, o onNext ira interar sobre o array, tratando cada elemento
        //individualmente
        Observable.fromIterable(listOfPessoas).subscribe( //it = pessoa
            { println(it.name) }, // onNext
            {  e -> Log.d(TAG,e.message.toString()) }, //onError
            { Log.d(TAG,"Completed!") } //onComplete

        )
    }
    
```

Ao usar arrays ou listas, cuidado com as operações _fromArray_ e _fromIterable_ do RXJava (o _from_ é usado para criar um observable no RXJava):

```
    private fun rxJavaTest2() {

        val listOfPessoas = listOf(
            Pessoa("Fran"),
            Pessoa("Sabrina"),
            Pessoa("Olesia")
        )


        //ao usar o fromIterable, o onNext ira interar sobre o array, tratando cada elemento
        //individualmente
        Observable.fromIterable(listOfPessoas).subscribe( //it = pessoa
            { println(it.name) }, // onNext
            {  e -> Log.d(TAG,e.message.toString()) }, //onError
            { Log.d(TAG,"Completed!") } //onComplete

        )

        println("--------------------------")

        //neste caso, ao usar o fromArray, o onNext ira tratar o array como um unico objeto
        Observable.fromArray(listOfPessoas).subscribe(
            { println(it) }, // onNext
            {  e -> Log.d(TAG,e.message.toString()) }, //onError
            { Log.d(TAG,"Completed!") } //onComplete
        )

    }


``` 

É possível criar manualmente um _observable_: 

```
    //E possivel criar um observable, tendo que implementar o onNex, onError e o onComplete
    private fun rxJavaTest4_createNewObservable(listOfPessoas: List<Pessoa>) =
        Observable.create<String> { emitter ->

            listOfPessoas.forEach{ pessoa ->
                if(pessoa.name.isNullOrEmpty())
                    emitter.onError(Throwable("Nome da pessoa esta em branco"))

                emitter.onNext(pessoa.name)

            }
            emitter.onComplete()

        }


    private fun rxJavaTest4() {

        val listOfPessoas = listOf(
            Pessoa("Fran"),
            Pessoa("Sabrina"),
            Pessoa("Olesia")
        )

        val pessoaObservable = rxJavaTest4_createNewObservable(listOfPessoas)
        pessoaObservable.subscribe(
            { println("Pessoa: $it") },
            { println("Ocorreu um erro") },
            { println("Processo completado!")}

        )
    }

```

O RXJava também conta com algums _operadores_, a saber: 

```

     /* Operadores */
    private fun rxJavaTest9() {
        //Map - Similar ao MAP do kotlin, serve para transformar cada elemento observado de acordo
        // com alguma funcao especifica

        Observable.fromIterable(DataSourceTest.listOfPessoas)
            .map {
                it to it.name?.length
            }
            .subscribe(
                {
                        println("Nome: ${it.first.name} - ${it.second}")
                }, //onNext

                { println("Ocorreu um erro")}, //onError
                { println("processo completado!") } //onComplete
            )
    }


    /* Operadores */
    private fun rxJavaTest10() {
        //Map - Similar ao MAP do kotlin, serve para transformar cada elemento observado de acordo
        // com alguma funcao especifica

        Observable.fromIterable(DataSourceTest.listOfPessoas)
            .map {
                mapPessoaToPessoaFisica(it.name!!,"001")
            }
            .subscribe(
                {
                    println(it)
                }, //onNext

                { println("Ocorreu um erro")}, //onError
                { println("processo completado!") } //onComplete
            )
    }



    private fun rxJavaTest11() {
        //Filter = Realiza um filtro para emitir somente os elementos que satisfacam determinada
        //condicao

        Observable.fromArray(1, 2, 3, 4, 5, 6, 7, 8)
            .filter { i -> i % 2 == 0 }
            .subscribe(
                { println(it) },
                { println("Ocorreu um erro!") },
                { println("Completado")}
            )
    }

```


Apesar de não estar descrito aqui, o RXJava conta também com algums tipos de emissores específicos, tal como o operador _Maybe_: 

>  Maybe (onSuccess ou onError ou onComplete).
>  Esse emissor é usado quando se deseja retornar um único valor opcional.
>  Seus métodos são mutuamente exclusivos, ou seja, apenas um deles é chamado.
>  Se o dado existir, o onSuccess será chamado; se não existir, o onComplete será invocado;
>  ou se um erro ocorrer, o onError é disparado.

```
     private fun rxJavaTest6() {
        val pessoa = Pessoa()

        Maybe.just(pessoa).subscribe(
            { println("Pessoa: ${it?.name}") }, //onNext
            { println("Ocorreu um erro") }, //onError
            { println("COmpletado") } //onComplete
        )
        // Perceba que o onComplete nao e chamado caso o onNext seja
    }


```


Sobre o RXJava, consulte [este artigo do Medium](https://nglauber.medium.com/introdu%C3%A7%C3%A3o-ao-rx-java-com-kotlin-90c58d184c6b).


No caso da implementação deste projeto, temos algo do tipo: 


```

    private fun <T> doSubscribeObservable(
        apiObservable: Observable<T>,
        listener: NetworkResultListener
    ){

            apiObservable.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                    //onNext
                    {
                        Log.d(TAG,"Data got in onNext: ${it.toString()}")
                        listener.onSuccess(it)
                    },

                    //onError
                    { throwable ->
                        Log.e(TAG,"Request Failure")
                        Log.e(TAG,throwable.message.toString())
                        Log.e(TAG,throwable.printStackTrace().toString())
                        listener.onFailure()
                    }
                )
    }


``` 

Atente para o _subscribeOn(Schedulers.io())_, que indica que o Observable será criado na thread de IO, e no _observeOn(AndroidSchedulers.mainThread())_, que indica que a entrega desses dados se dará na thread de UI.



### Koin 
 
O Koin é um framework responsável por auxiliar na injeção de dependências. 

> Injeção de Dependência é um padrão de projeto usado para evitar o alto nível de acoplamento de código dentro de uma aplicação. Sistemas com baixo acoplamento de código são melhores pelos seguintes motivos: Aumento na facilidade de manutenção/implementação de novas funcionalidades e também habilita a utilização de mocks para realizar unit testes.
> Em outras palavras, significa que a classe não é mais responsável por cirar os objetos dos quias ela depende.

Existem vários tipos de injeção de dependências, a saber:

 - **Injeção por construtor**: Acontece quando a classe a ser utilizada recebe por construtor uma instância de classe que ela mesmo irá utilizar. No desenvolvimento, este tipo de injeção é muito comum.
 
 - **Injeção por interface**: Ao invés de receber o construtor por parâmetro, agora a classe deve receber uma abstração da implementação que ela irá utilizar, através de uma interface. O grande benefício é que quem define qual implementação da abstração a ser utilizada, é quem está utilizando a classe principal.

 - **Injeção por propriedades**: Ocorre quando se tem a classe a ser injetada exposta como uma propriedade.

Com relação ao principal benefício de usar injeção de dependência via um framework ou lib é delegar a inicialização dessas dependências, permitindo assim o fornecimento automático das mesmas de acordo com o escopo necessário. 
Além disso, alguns momentos você precisará de determinadas implementações específicas, coisa que com um framework de injeção de dependências será transparente. Um exemplo disso é o ViewModelFactory, que com o Koin você não precisará implementar.


### Saved Instance 

É comum o uso da técnica de salvar a instância da view quando sua aplicação permite que a tela seja rotacionada. Quando o usuário rotaciona o dispositivo, a view tem que ser recriada, o que altera consequentemente o ciclo de vida. Uma forma de previnir que os dados sejam perdidos durante essa mudança de configuração da tela é salvando a instância da mesma. Uma outra técnica é a utilização de estruturas que façam com que os dados sobrevivam à esta configuração, como por exemplo o ViewModel. 

Atenção: este projeto em questão não utiliza ViewModel.  
  
No caso do uso de fragments, é necessário manter a instância de cada fragment bem como a instância dos dados que estes fragments possuem. Para tanto: 


```
    class MainActivity : AppCompatActivity() {
    
         private var oneJokeFragment = OneJokeFragment()
         private var searchJokesFragment = SearchJokeFragment()


        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            binding = ActivityMainBinding.inflate(layoutInflater)
            setContentView(binding.root)

            if(savedInstanceState != null){ //verifica se consegue resgatar as instâncias dos fragments
                Log.d(TAG,"Restoring fragments instances...")
                oneJokeFragment = supportFragmentManager.getFragment(savedInstanceState,ONE_JOKE_FRAG_NAME) as OneJokeFragment
                searchJokesFragment = supportFragmentManager.getFragment(savedInstanceState,SEARCH_JOKE_FRAG_NAME) as SearchJokeFragment

            }else{
                addAllFragments() //adiciona os fragments, faz replace, chamada de serviço, etc.
            }
        }
 

        //salva as instancias dos fragments
        override fun onSaveInstanceState(outState: Bundle) {
            supportFragmentManager.putFragment(outState,ONE_JOKE_FRAG_NAME,oneJokeFragment)
            supportFragmentManager.putFragment(outState,SEARCH_JOKE_FRAG_NAME,searchJokesFragment)
            super.onSaveInstanceState(outState)
        }

   
    }


```


Já nos fragments, temos:

```
    class fragment: Fragment(){

        var dataToSave: Serializable? = null
        
        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            binding = getViewBinding()
           

            //tenta restaurar os dados
            if(savedInstanceState != null){
                Log.d(TAG,"Returning data from saved instance")
                dataToSave = savedInstanceState
                    .getSerializable(ONE_JOKE_FRAG_SAVED_DATA_KEY) as Joke
                showJoke()
            }else{
                oneJokePresenter.requestForOneJoke()
            }

            return binding.root
        }

        
        //salva os dados
        override fun onSaveInstanceState(outState: Bundle) {
            super.onSaveInstanceState(outState)
            outState.putSerializable(fragTag,dataToSave)
        }
    }

```

Pelo ciclo de vida da activity/fragment, toda vez que a tela é girada, o método _onSaveInstanceState()_  é chamado e em seguida o _onCreate()_ (e o _onCreateView()_ no caso dos fragments) é chamado. 

**Importante**: Quando se utiliza o _replace()_ através do Fragment Transaction, a instância do fragment é destruída, logo os dados que estão salvos nessa instância deste determinado fragment também serão destruidos. Logo este comportamento poderá ocasionar um bug.


## Bugs conhecidos

Este aplicativo apresenta um bug supostamente relacionado ao ciclo de vida: ambos os fragments são criados e adicionados (é dado um _add()_ e não um _replace()_ através do fragment transaction). Há suporte ao saved instance para permitir que o usuário consiga rotacionar a tela e os dados de cada fragment permanecam os mesmos. O Bug ocorre nesta rotação, onde a view perde a capacidade de responder à ações do usuário.



