package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.service

import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.models.Joke
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.models.ListOfJokes
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface EndPointService {

    /**
     * Solicita da API um único Joke, que será gerado randomicamente por ela.
     * @return Response<Joke?>
     * */
    @GET("random")
    fun getRandonFunFact(): Observable<Joke?>



    /**
     * Solicita da API uma lista de categorias de jokes.
     * @return Response<List<String>>
     * */
    @GET("categories")
    fun getListOfCategories(): Observable<List<String>?>



    /**
     * Solicita da API um joke baseado em uma categoria específica.
     * @param category: String
     * @return Response<Joke?>
     * */
    @GET("random?")
    fun queryFunFactByCategory(
        @Query("category") category: String): Observable<Joke?>




    /**
     * Solicita da API uma lista de Jokes que são filtrados com base em uma
     * pesquisa feito pelo usuário.
     * @param queryText: String
     * @return Response<ListOfJokes?>
     * */
    @GET("search?")
    fun queryFunFactByText(
        @Query("query") queryText: String): Observable<ListOfJokes?>



}