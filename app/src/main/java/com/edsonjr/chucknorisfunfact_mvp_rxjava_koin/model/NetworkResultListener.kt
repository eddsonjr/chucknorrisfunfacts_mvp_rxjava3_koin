package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model


interface NetworkResultListener {
    fun <T> onSuccess(data: T)
    fun onFailure()
}
