package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.datasource

import android.util.Log
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.service.EndPointService
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.models.Joke
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.models.ListOfJokes
import io.reactivex.rxjava3.core.Observable

class BaseDataSourceImpl(private val api: EndPointService): BaseDataSource {

    private val TAG = this.javaClass.name

    override fun getOneJoke(): Observable<Joke?> {
        Log.d(TAG,"Requesting API for One Joke...")
        return api.getRandonFunFact()
    }

    override fun getCategoriesOfJokes(): Observable<List<String>?> {
        Log.d(TAG,"Requesting API for Joke Categories...")
       return api.getListOfCategories()
    }

    override fun queryFunFactByCategory(category: String): Observable<Joke?> {
        Log.d(TAG,"Quering Fun Facts by Category. Category selected: $category")
       return api.queryFunFactByCategory(category)
    }

    override fun queryFunFactByText(queryText: String): Observable<ListOfJokes?> {
        return api.queryFunFactByText(queryText)
    }


}