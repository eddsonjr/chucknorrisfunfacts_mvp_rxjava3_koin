package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class Joke(

    @SerializedName("value")
    val texto: String,

    @SerializedName("categories")
    val categorias: List<String>,

    val id: String,
    val icon_url: String
): Serializable