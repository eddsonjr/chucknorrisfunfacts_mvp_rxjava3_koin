package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model

interface NetworkNotFoundResultListener {
    fun notFoundListener()
}