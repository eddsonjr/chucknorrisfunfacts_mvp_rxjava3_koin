package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.models

import java.io.Serializable

data class ListOfJokes(val total: Int, val result: List<Joke>):Serializable