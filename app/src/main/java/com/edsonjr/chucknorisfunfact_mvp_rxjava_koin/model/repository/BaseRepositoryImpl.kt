package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.repository

import android.util.Log
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.NetworkNotFoundResultListener
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.NetworkResultListener
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.datasource.BaseDataSource
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.models.ListOfJokes
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.lang.ClassCastException

class BaseRepositoryImpl(private val dataSource: BaseDataSource): BaseRepository {

    private val TAG = this.javaClass.name

    override fun getOneJoke(listener: NetworkResultListener) {
        doSubscribeObservable(dataSource.getOneJoke(),listener)
    }

    override fun getCategoriesOfJokes(listener: NetworkResultListener) {
        doSubscribeObservable(dataSource.getCategoriesOfJokes(),listener)
    }

    override fun queryFunFactByCategory(category: String, listener: NetworkResultListener) {
        doSubscribeObservable(dataSource.queryFunFactByCategory(category),listener)
    }

    override fun queryFunFactByText(
        queryText: String,
        listener: NetworkResultListener,
        notFoundListener: NetworkNotFoundResultListener) {

        doSearchSubscribeObservable(dataSource.queryFunFactByText(queryText),
            listener,notFoundListener)
    }




    private fun <T> doSubscribeObservable(
        apiObservable: Observable<T>,
        listener: NetworkResultListener
    ){

            apiObservable.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                    //onNext
                    {
                        Log.d(TAG,"Data got in onNext: ${it.toString()}")
                        listener.onSuccess(it)
                    },

                    //onError
                    { throwable ->
                        Log.e(TAG,"Request Failure")
                        Log.e(TAG,throwable.message.toString())
                        Log.e(TAG,throwable.printStackTrace().toString())
                        listener.onFailure()
                    }
                )
    }


    private fun <T> doSearchSubscribeObservable(
        apiObservable: Observable<T>,
        listener: NetworkResultListener,
        notFoundResultListener: NetworkNotFoundResultListener
    ){

        apiObservable.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                //onNext
                {
                    Log.d(TAG,"Data got in onNext: ${it.toString()}")
                    try{
                        val result = it as ListOfJokes

                        if(result.total == 0){
                            notFoundResultListener.notFoundListener()
                        }else{
                            listener.onSuccess(it)
                        }
                    }catch (cast: TypeCastException){
                        Log.e(TAG,"Cast Error")
                        Log.e(TAG,cast.message.toString())
                        listener.onFailure()
                    }catch (cast: ClassCastException){
                        Log.e(TAG,"Cast Error")
                        Log.e(TAG,cast.message.toString())
                        listener.onFailure()
                    }
                },

                //onError
                { throwable ->
                    Log.e(TAG,"Request Failure")
                    Log.e(TAG,throwable.message.toString())
                    Log.e(TAG,throwable.printStackTrace().toString())
                    listener.onFailure()
                }
            )
    }

}
