package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.repository

import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.NetworkNotFoundResultListener
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.NetworkResultListener


interface BaseRepository {

    fun getOneJoke(listener: NetworkResultListener)

    fun getCategoriesOfJokes(listener: NetworkResultListener)

    fun queryFunFactByCategory(category: String,listener: NetworkResultListener)

    fun queryFunFactByText(queryText: String,listener: NetworkResultListener,
                           notFoundListener: NetworkNotFoundResultListener)
}