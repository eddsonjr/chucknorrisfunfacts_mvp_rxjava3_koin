package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.datasource

import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.models.Joke
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.models.ListOfJokes
import io.reactivex.rxjava3.core.Observable

interface BaseDataSource {

    fun getOneJoke(): Observable<Joke?>
    fun getCategoriesOfJokes(): Observable<List<String>?>
    fun queryFunFactByCategory(category: String): Observable<Joke?>
    fun queryFunFactByText(queryText: String): Observable<ListOfJokes?>
}