package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.di

import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.datasource.BaseDataSource
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.datasource.BaseDataSourceImpl
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.repository.BaseRepository
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.repository.BaseRepositoryImpl
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter.GetJokesByCategoryPresenterImpl
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter.GetListOfCategoriesPresenterImpl
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter.GetOneJokePresenterImpl
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter.SearchForJokesPresenterImpl
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.service.EndPointService
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.service.RetrofitService
import org.koin.dsl.module
import retrofit2.Retrofit

object Di {

    private val retrofitModule = module {
        single { RetrofitService().initRetrofit()}
        single <EndPointService> {get<Retrofit>().create(EndPointService::class.java)}
    }


    private val dataSourceModule = module {
        factory <BaseDataSource> { BaseDataSourceImpl(get()) }
    }

    private val repositoryModule = module {
        factory <BaseRepository> { BaseRepositoryImpl(get()) }
    }


    private val presenterModule = module {
        single { GetOneJokePresenterImpl(get(),get()) }
        single { GetListOfCategoriesPresenterImpl(get(),get()) }
        single { GetJokesByCategoryPresenterImpl(get(),get()) }
        single { SearchForJokesPresenterImpl(get(),get(),get()) }
    }


    val appModules = listOf(
        retrofitModule,
        dataSourceModule,
        repositoryModule,
        presenterModule
    )

}