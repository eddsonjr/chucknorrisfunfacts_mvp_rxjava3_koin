package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.di

import android.app.Application
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.di.Di.appModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        // start Koin!
        startKoin {
            // declare used Android context
            androidContext(this@MyApplication)
            // declare modules
            modules(appModules)
        }
    }
}