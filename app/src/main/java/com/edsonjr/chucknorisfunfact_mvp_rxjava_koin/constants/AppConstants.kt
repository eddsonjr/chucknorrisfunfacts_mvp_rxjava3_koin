package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.constants

class AppConstants {
    companion object {

        const val TOOLBAR_TITLE = "Jokes"

        const val ONE_JOKE_FRAG_SAVED_DATA_KEY = "one_joke_frag_data_saved_instance_key"
        const val SEARCH_FRAG_SAVED_DATA_KEY = "search_frag_data_saved_instance_key"

        const val ONE_JOKE_FRAG_NAME = "oneJokeFrag"
        const val SEARCH_JOKE_FRAG_NAME = "searchJokeFrag"

        const val DIALOG_FRAGMENT_TAG = "list_of_categories_dialog_fragment"

    }
}