package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.R
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.databinding.FragmentListOfJokesCategoriesBinding
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter.BaseViewPresenter
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter.GetListOfCategoriesPresenterImpl
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter.SelectedCategoryListener
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.ui.adapter.ListOfCategoriesAdapter
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf


class ListOfJokesCategoriesFragment() :
    DialogFragment(), BaseViewPresenter {

    private val TAG = this.javaClass.name

    private val presenter: GetListOfCategoriesPresenterImpl by inject {parametersOf(this)}

    private var _binding: FragmentListOfJokesCategoriesBinding? = null
    private val binding get() = _binding!!

    private val adapter = ListOfCategoriesAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentListOfJokesCategoriesBinding.inflate(layoutInflater)
        initListeners()
        presenter.requestForCategoryList()
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        val currentDialog = dialog
        if(currentDialog != null){
            val width = resources.getDimensionPixelSize(R.dimen.dialogFragmentWidth)
            val height = resources.getDimensionPixelSize(R.dimen.dialogFragmentHeight)
            currentDialog.window?.setLayout(width,height)
        }
    }


    override fun showLoading() {
        binding.categoriesRecyclerview.visibility = View.INVISIBLE
        binding.progressbar.visibility = View.VISIBLE
        binding.errorTextView.visibility = View.GONE
    }

    override fun hideLoading() {
        binding.progressbar.visibility = View.GONE
    }

    override fun showError() {
        binding.categoriesRecyclerview.visibility = View.INVISIBLE
        binding.progressbar.visibility = View.GONE
        binding.errorTextView.visibility = View.VISIBLE
    }

    override fun hideError() {
        binding.errorTextView.visibility = View.GONE
    }

    override fun <T> showSuccess(data: T) {
        val listOfCategories = data as List<String>
        setupRecyclerView(listOfCategories)
    }

    private fun setupRecyclerView(categoryList: List<String>) {
        binding.categoriesRecyclerview.visibility = View.VISIBLE
        binding.categoriesRecyclerview.adapter = adapter
        binding.categoriesRecyclerview.layoutManager = LinearLayoutManager(requireContext())
        adapter.updateRecyclerView(categoryList)

        adapter.searchJokesByCategoryListener = { category ->
            Log.d(TAG,"Category selected: $category")
            SelectedCategoryListener.selectedCategoryListener.value = category
            this.dismiss()
        }
    }

    private fun initListeners() {
        binding.closeButton.setOnClickListener {
            this.dismiss()
        }
    }
}