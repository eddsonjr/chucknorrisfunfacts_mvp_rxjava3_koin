package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.databinding.JokeQuoteCellBinding
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.models.Joke
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.models.ListOfJokes


class ListOfJokesConsultedRecyclerViewAdapter: RecyclerView.Adapter<ListOfJokesConsultedRecyclerViewAdapter.ViewHolder>() {


    private var listOfJokes: List<Joke> = listOf()

    inner class ViewHolder(var binding: JokeQuoteCellBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(jokeQuote: String){
            binding.TVJokeQuoteCell.text = jokeQuote
        }

    }


    fun updateRecyclerView(listOfJokes: ListOfJokes){
        this.listOfJokes = listOfJokes.result
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = JokeQuoteCellBinding.inflate(layoutInflater,parent,false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listOfJokes[position].texto)
    }

    override fun getItemCount(): Int = listOfJokes.size
}