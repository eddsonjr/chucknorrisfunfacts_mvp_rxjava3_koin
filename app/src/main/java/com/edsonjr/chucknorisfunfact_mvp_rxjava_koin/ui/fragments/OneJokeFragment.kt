package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.ui.fragments


import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import android.widget.Toolbar
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.R
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.constants.AppConstants.Companion.DIALOG_FRAGMENT_TAG
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.constants.AppConstants.Companion.ONE_JOKE_FRAG_SAVED_DATA_KEY
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.constants.AppConstants.Companion.TOOLBAR_TITLE
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.databinding.FragmentOneJokeBinding
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.models.Joke
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter.GetJokesByCategoryPresenterImpl
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter.GetOneJokePresenterImpl
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter.SelectedCategoryListener
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf


class OneJokeFragment(): BaseFragment<FragmentOneJokeBinding>(ONE_JOKE_FRAG_SAVED_DATA_KEY){

    private val TAG = this.javaClass.name

    private val oneJokePresenter: GetOneJokePresenterImpl by inject { parametersOf(this)}

    private val jokesByCategoryPresenter: GetJokesByCategoryPresenterImpl
        by inject { parametersOf(this)}

    private var dialogFragmentInstance = ListOfJokesCategoriesFragment()

    override fun onResume() {
        super.onResume()
        observeCategorySelected()
        binding?.newJokeButton?.setOnClickListener {
            Log.d(TAG,"User want a new joke")
            oneJokePresenter.requestForOneJoke()
        }
    }


    override fun getViewBinding(): FragmentOneJokeBinding = FragmentOneJokeBinding
        .inflate(layoutInflater)


    override fun executeWhenOnViewCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true);
        initToolbar()

        if(savedInstanceState != null){
            Log.d(TAG,"Returning data from saved instance")
            dataToSave = savedInstanceState
                .getSerializable(ONE_JOKE_FRAG_SAVED_DATA_KEY) as Joke
            showJoke()
        }else{
            oneJokePresenter.requestForOneJoke()
        }
    }


    override fun <T> setupUIToSuccess(data: T) {
        dataToSave = data as Joke
        showJoke()
    }

    override fun setupUIToError() {
        Log.d(TAG,"Setup UI ERROR...")
        binding?.jokeTextView?.visibility = View.GONE
        binding?.erroFramelayout?.visibility = View.VISIBLE
    }



    override fun removeError() {
        binding?.erroFramelayout?.visibility = View.GONE
        binding?.jokeTextView?.visibility = View.VISIBLE
    }

    override fun setupLoading() {
        binding?.jokeTextView?.visibility = View.GONE
        binding?.progressbar?.visibility = View.VISIBLE

    }

    override fun removeLoading() {
        binding?.progressbar?.visibility = View.GONE
        binding?.jokeTextView?.visibility = View.VISIBLE
    }


    private fun showJoke() {
        binding?.jokeTextView?.text = (dataToSave as Joke).texto
    }


    private fun initToolbar() {
        val rootView = binding?.root
        val toolbar = rootView?.findViewById<Toolbar>(R.id.jokes_toolbar)
        toolbar?.inflateMenu(R.menu.joke_config_menu)
        toolbar?.title = TOOLBAR_TITLE
        requireActivity().setActionBar(toolbar)

    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.joke_config_menu, menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId)
        {
            R.id.joke_category_list_menu -> {
                Log.d(TAG,"Select joke category. Opening dialog fragment...")
                dialogFragmentInstance.show(parentFragmentManager, DIALOG_FRAGMENT_TAG)
                return true
            }

            R.id.joke_clear_category_selection_menu -> {
                Log.d(TAG,"Clear joke category selection. Calling for random joke again...")

                oneJokePresenter.requestForOneJoke()
                SelectedCategoryListener.selectedCategoryListener.value = null

                binding?.selectedCategoryTextView?.text = ""
                binding?.selectedCategoryTextView?.visibility = View.INVISIBLE

                Toast.makeText(requireContext(),getString(R.string.random_jokes_toast),
                    Toast.LENGTH_SHORT).show()

                return true

            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    private fun observeCategorySelected() {
        SelectedCategoryListener.selectedCategoryListener.observe(viewLifecycleOwner){ category ->
            Log.d(TAG,"Trying load jokes of category $category")
            if(!category.isNullOrEmpty()){
                binding?.selectedCategoryTextView?.visibility = View.VISIBLE
                binding?.selectedCategoryTextView?.text = getString(R.string.category) +  category
                jokesByCategoryPresenter.requestForJokeByCategory(category)

            }
        }
    }
}