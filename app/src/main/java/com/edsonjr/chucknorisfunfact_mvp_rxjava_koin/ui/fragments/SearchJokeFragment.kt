package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toolbar
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.R
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.constants.AppConstants.Companion.SEARCH_FRAG_SAVED_DATA_KEY
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.constants.AppConstants.Companion.TOOLBAR_TITLE
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.databinding.FragmentSearchJokeBinding
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.models.ListOfJokes
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter.NotFoundViewPresenter
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter.SearchForJokesPresenterImpl
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.ui.adapter.ListOfJokesConsultedRecyclerViewAdapter
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf


class SearchJokeFragment : BaseFragment<FragmentSearchJokeBinding>(SEARCH_FRAG_SAVED_DATA_KEY),
    NotFoundViewPresenter {

    private val TAG = this.javaClass.name

    private lateinit var adapter: ListOfJokesConsultedRecyclerViewAdapter

    private val presenter: SearchForJokesPresenterImpl by inject { parametersOf(this)}


    override fun <T> setupUIToSuccess(data: T) {
         dataToSave = data as ListOfJokes
        if((dataToSave as ListOfJokes).total == 0){
            Log.d(TAG, "Not found!")
            showNotFound()
        }else{
            hideNotFound()
            binding?.consultedJokesRecycleview?.visibility = View.VISIBLE
            dataToSave?.let {
                adapter.updateRecyclerView(it as ListOfJokes)
            }
        }
    }

    override fun setupUIToError() {
        Log.d(TAG,"Setup UI ERROR...")
        binding?.erroFramelayout?.visibility = View.VISIBLE
    }


    override fun removeError() {
        binding?.erroFramelayout?.visibility = View.GONE

    }

    override fun setupLoading() {
        binding?.progressbar?.visibility = View.VISIBLE
        binding?.consultedJokesRecycleview?.visibility = View.GONE
        hideNotFound()
    }

    override fun removeLoading() {
        binding?.progressbar?.visibility = View.GONE
        binding?.consultedJokesRecycleview?.visibility = View.VISIBLE
    }

    override fun showNotFound() {
        binding?.consultedJokesRecycleview?.visibility = View.GONE
        binding?.erroFramelayout?.visibility = View.GONE
        binding?.notFoundFrameLayout?.visibility = View.VISIBLE
    }

    override fun hideNotFound() {
        binding?.notFoundFrameLayout?.visibility = View.GONE
    }


    private fun initToolbar() {
        val rootView = binding?.root
        val toolbar = rootView?.findViewById<Toolbar>(R.id.search_joke_toolbar)
        toolbar?.title = TOOLBAR_TITLE
        requireActivity().setActionBar(toolbar)
    }


    private fun initSearchBarListeners() {
        binding?.searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    Log.d(TAG,"User searching for: $query")
                    presenter.searchJokesByText(query)
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }

        })
    }


    private fun initRecyclerView() {
        adapter = ListOfJokesConsultedRecyclerViewAdapter()
        binding?.consultedJokesRecycleview?.adapter = adapter
        binding?.consultedJokesRecycleview?.layoutManager = LinearLayoutManager(requireContext())
    }

    override fun getViewBinding(): FragmentSearchJokeBinding =
        FragmentSearchJokeBinding.inflate(layoutInflater)

    override fun executeWhenOnViewCreate(savedInstanceState: Bundle?) {
        initToolbar()
        initSearchBarListeners()
        initRecyclerView()

        if(savedInstanceState != null) {
            Log.d(TAG, "Returning data from saved instance")
            dataToSave = savedInstanceState.getSerializable(SEARCH_FRAG_SAVED_DATA_KEY) as ListOfJokes?
            dataToSave?.let {
                adapter.updateRecyclerView(it as ListOfJokes)
            }
        }

    }


}