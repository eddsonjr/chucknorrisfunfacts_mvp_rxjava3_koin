package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.FragmentTransaction
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.R
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.constants.AppConstants.Companion.ONE_JOKE_FRAG_NAME
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.constants.AppConstants.Companion.SEARCH_JOKE_FRAG_NAME
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.databinding.ActivityMainBinding
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter.*
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.ui.fragments.OneJokeFragment
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.ui.fragments.SearchJokeFragment

class MainActivity : AppCompatActivity() {

    private val TAG = this.javaClass.name
    private lateinit var binding: ActivityMainBinding

    private var oneJokeFragment = OneJokeFragment()
    private var searchJokesFragment = SearchJokeFragment()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if(savedInstanceState != null){
            Log.d(TAG,"Restoring fragments instances...")
            oneJokeFragment = supportFragmentManager.getFragment(savedInstanceState,ONE_JOKE_FRAG_NAME) as OneJokeFragment
            searchJokesFragment = supportFragmentManager.getFragment(savedInstanceState,SEARCH_JOKE_FRAG_NAME) as SearchJokeFragment

        }else{
            addAllFragments()
        }

        //bottom menu item click listener
        binding.bottomNavigation.setOnItemSelectedListener { menuId ->
            when(menuId.itemId){
                R.id.joke_bottom_menu_item -> {
                    Log.d(TAG,"Selected main fragment!")
                    showOneJokeFragment()
                }
                R.id.joke_bottom_menu_search -> {
                    Log.d(TAG,"Selected search fro jokes fragment")
                    showSearchJokeFragment()
                }
            }
            return@setOnItemSelectedListener true
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        supportFragmentManager.putFragment(outState,ONE_JOKE_FRAG_NAME,oneJokeFragment)
        supportFragmentManager.putFragment(outState,SEARCH_JOKE_FRAG_NAME,searchJokesFragment)
        super.onSaveInstanceState(outState)
    }


    private fun showOneJokeFragment() {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()

        fragmentTransaction.hide(this.searchJokesFragment)
        fragmentTransaction.show(this.oneJokeFragment)
            .commit()
    }

    private fun showSearchJokeFragment() {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.hide(this.oneJokeFragment)
        fragmentTransaction.show(this.searchJokesFragment)
            .commit()
    }

    private fun addAllFragments() {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.fragment_container,this.oneJokeFragment)
        fragmentTransaction.add(R.id.fragment_container,this.searchJokesFragment)
        fragmentTransaction.hide(this.searchJokesFragment)
            .commit()
    }


}
