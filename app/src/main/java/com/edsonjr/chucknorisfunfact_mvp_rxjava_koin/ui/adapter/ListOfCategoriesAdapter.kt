package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.databinding.JokesCategoriesItemCellBinding

class ListOfCategoriesAdapter:  RecyclerView.Adapter<ListOfCategoriesAdapter.ViewHolder>() {

    private var listOfJokesCategories: List<String>? = null
    var searchJokesByCategoryListener: (String) -> Unit = {}


    inner class ViewHolder(var binding: JokesCategoriesItemCellBinding): RecyclerView.ViewHolder(binding.root){

        fun bind(categoryName: String){
            binding.TVJokeCategoryNameListItem.text = categoryName

            binding.root.setOnClickListener {
                searchJokesByCategoryListener(categoryName)
            }
        }
    }


    fun updateRecyclerView(jokesCategories: List<String>){
        this.listOfJokesCategories = null
        this.listOfJokesCategories = jokesCategories
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = JokesCategoriesItemCellBinding.inflate(layoutInflater,parent,false)
        return ViewHolder(binding)
    }



    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listOfJokesCategories?.get(position)!!)

    }

    override fun getItemCount(): Int {
        return listOfJokesCategories?.size!!
    }
}