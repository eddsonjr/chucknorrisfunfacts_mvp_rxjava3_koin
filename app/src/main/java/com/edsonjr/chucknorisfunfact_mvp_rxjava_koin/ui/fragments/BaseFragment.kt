package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.motion.utils.ViewState
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter.BaseViewPresenter
import java.io.Serializable

abstract class BaseFragment<VB: ViewBinding>(private val fragTag: String): Fragment(),
    BaseViewPresenter {

    private val TAG = this.javaClass.name
    lateinit var binding: VB

    var dataToSave: Serializable? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = getViewBinding()
        executeWhenOnViewCreate(savedInstanceState)
        return binding.root
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(fragTag,dataToSave)
    }

    override fun showLoading() {
        Log.d(TAG,"Loding...")
        setupLoading()
    }

    override fun hideLoading() {
        Log.d(TAG,"Hiding loading progress bar...")
        removeLoading()
    }

    override fun showError() {
        Log.d(TAG,"Request fail!. Showing the error screen...")
        hideLoading()
        setupUIToError()
    }

    override fun hideError() {
        Log.d(TAG,"Hiding error screen...")
        removeError()
    }

    override fun <T> showSuccess(data: T) {
        Log.d(TAG,"Request returned successfully...")
        Log.d(TAG,"Data: ${data.toString()}")
        hideLoading()
        hideError()
        setupUIToSuccess(data)
    }


    abstract fun <T> setupUIToSuccess(data: T)
    abstract fun setupUIToError()
    abstract fun removeError()
    abstract fun setupLoading()
    abstract fun removeLoading()
    abstract fun getViewBinding(): VB
    abstract fun executeWhenOnViewCreate(savedInstanceState: Bundle?)
}