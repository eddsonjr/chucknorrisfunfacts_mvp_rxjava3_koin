package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter

import androidx.lifecycle.MutableLiveData

class SelectedCategoryListener {
    companion object {
        val selectedCategoryListener: MutableLiveData<String?> = MutableLiveData()
    }
}