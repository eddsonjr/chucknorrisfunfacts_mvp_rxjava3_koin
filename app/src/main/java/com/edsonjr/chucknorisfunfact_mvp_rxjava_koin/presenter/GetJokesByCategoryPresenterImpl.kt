package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter

import android.util.Log
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.NetworkResultListener
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.repository.BaseRepository

class GetJokesByCategoryPresenterImpl(
    override val view: BaseViewPresenter,
    private val repository: BaseRepository)
    : GetJokesByCategoryPresenter,NetworkResultListener {


    private val TAG = this.javaClass.name

    override fun <T> onSuccess(data: T) {
        Log.d(TAG,"Request for jokes by category returned successfully. Data: ${data.toString()}")
        view.hideError()
        view.hideLoading()
        view.showSuccess(data)
    }

    override fun onFailure() {
        Log.e(TAG,"Request for jokes by category failed!")
        view.hideLoading()
        view.showError()

    }

    override fun requestForJokeByCategory(category: String) {
        Log.d(TAG,"Requesting repository for jokes by category")
        view.showLoading()
        repository.queryFunFactByCategory(category,this)
    }
}