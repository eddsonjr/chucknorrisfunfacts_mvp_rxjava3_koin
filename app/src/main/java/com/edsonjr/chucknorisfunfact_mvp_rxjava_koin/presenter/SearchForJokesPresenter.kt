package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter

interface SearchForJokesPresenter {
    val view: BaseViewPresenter
    val notFoundView: NotFoundViewPresenter
    fun searchJokesByText(queryText: String)
}