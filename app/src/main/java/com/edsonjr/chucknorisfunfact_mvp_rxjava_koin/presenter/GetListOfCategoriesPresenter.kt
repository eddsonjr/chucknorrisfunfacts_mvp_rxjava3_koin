package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter

interface GetListOfCategoriesPresenter {
    val view: BaseViewPresenter
    fun requestForCategoryList()

}