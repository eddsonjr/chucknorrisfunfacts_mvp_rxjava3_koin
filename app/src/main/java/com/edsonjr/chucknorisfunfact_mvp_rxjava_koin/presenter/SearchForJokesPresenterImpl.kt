package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter

import android.util.Log
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.NetworkNotFoundResultListener
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.NetworkResultListener
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.models.ListOfJokes
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.repository.BaseRepository

class SearchForJokesPresenterImpl(
    override val view: BaseViewPresenter,
    override val notFoundView: NotFoundViewPresenter,
    private val repository: BaseRepository):
        SearchForJokesPresenter,NetworkResultListener,NetworkNotFoundResultListener {

    private val TAG = this.javaClass.name

    override fun <T> onSuccess(data: T) {
        Log.d(TAG,"Searching jokes for text. Data: ${data.toString()}")
        view.hideLoading()
        view.hideError()
        notFoundView.hideNotFound()
        view.showSuccess(data)

    }

    override fun onFailure() {
        Log.e(TAG,"Search for jokes failed!")
        view.hideLoading()
        notFoundView.hideNotFound()
        view.showError()
    }

    override fun searchJokesByText(queryText: String) {
        Log.d(TAG,"Searching for jokes. Query: $queryText")
        view.showLoading()
        view.hideError()
        notFoundView.hideNotFound()
        repository.queryFunFactByText(
            queryText = queryText,
            listener = this,
            notFoundListener = this
        )
    }

    override fun notFoundListener() {
        Log.d(TAG,"Not found!")
        view.hideLoading()
        view.hideError()
        notFoundView.showNotFound()
    }
}