package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter

interface GetOneJokePresenter {
    val view: BaseViewPresenter
    fun requestForOneJoke()
}