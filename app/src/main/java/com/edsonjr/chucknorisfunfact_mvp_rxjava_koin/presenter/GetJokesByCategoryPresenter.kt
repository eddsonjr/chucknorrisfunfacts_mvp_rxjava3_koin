package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter

interface GetJokesByCategoryPresenter {
    val view: BaseViewPresenter
    fun requestForJokeByCategory(category: String)
}