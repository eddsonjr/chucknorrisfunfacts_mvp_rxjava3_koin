package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter

import android.util.Log
import androidx.lifecycle.LifecycleObserver
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.NetworkResultListener
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.repository.BaseRepository

class GetListOfCategoriesPresenterImpl(
    override val view: BaseViewPresenter,
    private val repository: BaseRepository
    ) : GetListOfCategoriesPresenter, NetworkResultListener {

    private val TAG = this.javaClass.name

    override fun <T> onSuccess(data: T) {
        Log.d(TAG,"Request for list of categories returned successfully. Data: ${data.toString()}")
        view.hideLoading()
        view.hideError()
        view.showSuccess(data)
    }

    override fun onFailure() {
        Log.e(TAG,"Request for list of categories failed! ")
        view.hideLoading()
        view.showError()
    }

    override fun requestForCategoryList() {
        Log.d(TAG,"Requesting repository for list of jokes...")
        view.showLoading()
        repository.getCategoriesOfJokes(this)
    }


}