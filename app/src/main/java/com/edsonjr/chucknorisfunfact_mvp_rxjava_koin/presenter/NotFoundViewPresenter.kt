package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter

interface NotFoundViewPresenter {
    fun showNotFound()
    fun hideNotFound()
}