package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter

interface BaseViewPresenter {
    fun showLoading()
    fun hideLoading()
    fun showError()
    fun hideError()
    fun <T> showSuccess(data: T)
}