package com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.presenter

import android.util.Log
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.NetworkResultListener
import com.edsonjr.chucknorisfunfact_mvp_rxjava_koin.model.repository.BaseRepository

class GetOneJokePresenterImpl(
    override val view: BaseViewPresenter,
    private val repository: BaseRepository)
    : GetOneJokePresenter,NetworkResultListener {

    private val TAG = this.javaClass.name

    override fun <T> onSuccess(data: T) {
        Log.d(TAG,"Request for One Joke returned successfully. Data: ${data.toString()}")
        view.hideLoading()
        view.hideError()
        view.showSuccess(data)
    }

    override fun onFailure() {
        Log.e(TAG,"Request for One Joke failed! ")
        view.hideLoading()
        view.showError()

    }

    override fun requestForOneJoke() {
        Log.d(TAG,"Requesting repository for one joke...")
        view.hideError()
        view.showLoading()
        repository.getOneJoke(this)

    }
}